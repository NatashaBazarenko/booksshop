﻿namespace BooksShop
{
    public class Program
    {
        public static void Main(string[] args)
        {
            PrintHelloWorld();
        }

        private static void PrintHelloWorld()
        {
            Console.WriteLine("Hello World!");
        }
    }
}